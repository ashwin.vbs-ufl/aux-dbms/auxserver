<?php
require_once 'error/errorCodes.php';

class RestHandler{

	var $objectmap = array();

	function RestHandler(){
	}

	function rest_get(){
		http_response_code(404);
		return "GET is not implemented at the current path"; //TODO: change to exception
	}

	function rest_post(){
		http_response_code(404);
		return "POST is not implemented at the current path"; //TODO: change to exception
	}

	function rest_put(){
		http_response_code(404);
		return "PUT is not implemented at the current path"; //TODO: change to exception
	}

	function rest_delete(){
		http_response_code(404);
		return "DELETE is not implemented at the current path"; //TODO: change to exception
	}

	function handle($method, $pathvar, $token = null){
		if ($pathvar->isEmpty())
			switch($method){
			case 'GET':
				return $this->rest_get();
			case 'POST':
				return $this->rest_post();
			case 'PUT':
				return $this->rest_put();
			case 'DELETE':
				return $this->rest_delete();
			default:{
				http_response_code(404);
				return "unsupported method"; //TODO: change to exception
			}
			}

		$token = $pathvar->getNextToken();

		if (isset($this->objectmap[$token]))
			return $this->objectmap[$token]->handle($method, $pathvar);
		elseif (isset($this->objectmap['/default']))
			return $this->objectmap['/default']->handle($method, $pathvar, $token);
		else{
			http_response_code(404);
			return "path " . $token . " is not supported by the rest API"; //TODO: change to exception
		}
	}

}
?>
