#!/usr/bin/php

<?php
require './keyStore.php';
require '../Path.php';

echo $_SERVER['REQUEST_METHOD'] . " method in path " . $_REQUEST['path'];

$path = new PathParser($_REQUEST['path']);

 class base extends RestHandler{
 	function base(){
         $this->objectmap['keyStore'] = new keyStore();
 	}
 }

 $keystore = new base();

echo $keystore->handle($_SERVER['REQUEST_METHOD'], $path);
?>
