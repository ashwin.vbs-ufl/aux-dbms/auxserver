<?php
class PathParser{
	var $parsedpath = array();
	var $index = 0;
	var $read = 0;

	function PathParser($rawpath){
		$tok = strtok($rawpath, "/");
		while($tok !== false){
			$this->parsedpath[$this->index] = $tok;
			$this->index = $this->index + 1;
			$tok = strtok("/");
		}
	}

	function getNextToken(){
		if(!$this->isEmpty())
			$return = $this->parsedpath[$this->read];
		else
			$return = NULL;
		$this->read = $this->read + 1;
		return $return;
	}

	function isEmpty(){
		if($this->index > $this->read)
			return false;
		else
			return true;
	}
}
?>
