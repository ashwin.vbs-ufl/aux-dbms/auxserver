<?php
require 'error/errorCodes.php';

function fixIntegerColumns(&$results, $columns){
	foreach($results as $key => $value){
		foreach($columns as $column){
			settype($results[$key][$column], "integer");
		}
	}
}

function intIsSane($int){ //TODO: flesh out the function
	return true;
}

function stringIsSane($string){ //TODO: flesh out the function
	return true;
}

class Procedure{
	var $conn;
	var $stid;

	function Procedure($connection){
		$this->conn = $connection;
	}

	function processRequest($request){
		$this->stid = oci_parse($this->conn, $request);
	}

	function bind($token, &$variable, $length){
		oci_bind_by_name($this->stid, $token, $variable, $length);
	}

	function execute(){
		return oci_execute($this->stid);
	}

	function cleanup(){
		oci_free_statement($this->stid);
	}
}

class Query{
	var $conn;
	var $stid;

	function Query($connection){
		$this->conn = $connection;
	}

	function processRequest($request){
		$this->stid = oci_parse($this->conn, $request);
		return oci_execute($this->stid);
	}

	function getResultRecord(){
		return oci_fetch_array($this->stid, OCI_ASSOC+OCI_RETURN_NULLS);
	}

	function getResults($numrows = null){
		$num = oci_fetch_all($this->stid, $results, null, $numrows, OCI_FETCHSTATEMENT_BY_ROW);
		return $results;
	}

	function cleanup(){
		oci_free_statement($this->stid);
	}
}

class OCI{

	var $conn;

	function OCI($server, $user, $password){
		$this->conn = oci_connect($user, $password, $server);
	}

	function error(){
		if (!$this->conn) {
		    $e = oci_error();
			return $e['message'];
		} else {
			return "";
		}
	}

	function getQueryPrimitive(){
		$query = new Query($this->conn);
		return $query;
	}

	function getProcedurePrimitive(){
		$query = new Procedure($this->conn);
		return $query;
	}

	function cleanup(){
		oci_close($this->conn);
	}
}
?>
