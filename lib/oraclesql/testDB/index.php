#!/usr/bin/php
<?php
require_once 'librest/RestHandler.php';
require_once 'librest/Path.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

$path = new PathParser($_REQUEST['path']);

class base extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest('select * from test');
        $test = $request->getResults(2);

        fixIntegerColumns($test, array('ID', 'STAMP', 'ISTRUE'));

        $request->cleanup();
        $conn->cleanup();

        $json = new Services_JSON();
        return $json->encode($test);
    }
}

$keystore = new base();

echo $keystore->handle($_SERVER['REQUEST_METHOD'], $path);

?>
