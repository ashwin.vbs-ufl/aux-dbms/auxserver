<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class songFile extends RestHandler{
    var $querySongID;

    function rest_get(){
        return "$songurl";
    }

    function songFile($id){
        $this->querySongID = $id;
    }
}

class songAlbums extends RestHandler{
    var $querySongID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from songinstance where songinstance.songid = " . $this->querySongID);

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'SONGID', 'TRACKNUMBER'));

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function songAlbums($id){
        $this->querySongID = $id;
    }
}

class songArtists extends RestHandler{
    var $querySongID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from performance where performance.songid = " . $this->querySongID);

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'ARTISTID'));

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function songArtists($id){
        $this->querySongID = $id;
    }
}



class songByID extends RestHandler{
    var $songID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from song where songid = " . $this->songID);
        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'DATEOFPERFORMANCE'));

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function handle($method, $pathvar, $token){
        if(!intIsSane($token)){
            http_response_code(400);
            return "song id was not sanitary";
        }

        $this->songID = $token;

        if(!$pathvar->isEmpty()){
            $this->objectmap['file'] = new songFile($token);
            $this->objectmap['artists'] = new songArtists($token);
            $this->objectmap['albums'] = new songAlbums($token);
        }

        return parent::handle($method, $pathvar, $token);
    }
}

class song extends RestHandler{
    function song(){
        $this->objectmap['/default'] = new songByID();
    }

}
?>
