<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class ratingByID extends RestHandler{
    var $songID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from rating where userid = " . $_SESSION["USERID"] . " and songid = " . $this->songID);
        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'RATING'));

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function rest_put(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        if(!intIsSane($request_json['rating'])){
            http_response_code(400);
            return "input data was not sanitary";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getProcedurePrimitive();
        $request->processRequest("BEGIN upsert_rating(:userid, :songid, :rating); END;");

        $request->bind(":userid", $_SESSION["USERID"], 50);
        $request->bind(":songid", $this->songID, 38);
        $request->bind(":rating", $request_json['rating'], 38);

        $request->execute();
        $request->cleanup();
        $conn->cleanup();

        return true;
    }

    function handle($method, $pathvar, $token){
        if(!intIsSane($token)){
            http_response_code(400);
            return "song id was not sanitary";
        }

        $this->songID = $token;

        return parent::handle($method, $pathvar, $token);
    }
}


class globalRatingByID extends RestHandler{
    var $songID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select avg(rating) as rating, songid from rating group by songid having songid = " . $this->songID);
        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('RATING', 'SONGID'));

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function handle($method, $pathvar, $token){
        if(!intIsSane($token)){
            http_response_code(400);
            return "song id was not sanitary";
        }

        $this->songID = $token;

        return parent::handle($method, $pathvar, $token);
    }
}

class globalRating extends RestHandler{
    function globalRating(){
        $this->objectmap['/default'] = new globalRatingByID();
    }
}

class rating extends RestHandler{
    function rating(){
        $this->objectmap['/default'] = new ratingByID();
        $this->objectmap['global'] = new globalRating();
    }
}
?>
