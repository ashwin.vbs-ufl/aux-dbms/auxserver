<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class artistArt extends RestHandler{
    var $queryArtistID;

    function rest_get(){
        return "$artisturl";
    }

    function artistArt($id){
        $this->queryArtistID = $id;
    }
}

class artistSongs extends RestHandler{
    var $queryArtistID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from performance where performance.artistid = " . $this->queryArtistID);

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'ARTISTID'));

        http_response_code(200);

        return $results;
    }

    function artistSongs($id){
        $this->queryArtistID = $id;
    }
}

class artistAlbums extends RestHandler{
    var $queryArtistID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from album where album.artistid = " . $this->queryArtistID);

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'ARTISTID', 'YEAR'));

        http_response_code(200);
        return $results;
    }

    function artistAlbums($id){
        $this->queryArtistID = $id;
    }
}

class artistByID extends RestHandler{
    var $artistID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from artist where artistid = " . $this->artistID);
        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ARTISTID', 'DOB'));
        http_response_code(200);
        return $results;
    }

    function handle($method, $pathvar, $token){
        if(!intIsSane($token)){
            http_response_code(400);
            return "artist id was not sanitary";
        }

        $this->artistID = $token;

        if(!$pathvar->isEmpty()){
            $this->objectmap['art'] = new artistArt($token);
            $this->objectmap['songs'] = new artistSongs($token);
            $this->objectmap['albums'] = new artistAlbums($token);
        }

        return parent::handle($method, $pathvar, $token);
    }
}

class artist extends RestHandler{
    function artist(){
        $this->objectmap['/default'] = new artistByID();
    }
}
?>
