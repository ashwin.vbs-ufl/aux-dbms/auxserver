<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class albumArt extends RestHandler{
    var $queryAlbumID;

    function rest_get(){
        return "$albumurl";
    }

    function albumArt($id){
        $this->queryAlbumID = $id;
    }
}

class albumSongs extends RestHandler{
    var $queryAlbumID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from songinstance where songinstance.albumid = " . $this->queryAlbumID);

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'SONGID', 'TRACKNUMBER'));

        http_response_code(200);

        return $results;
    }

    function albumSongs($id){
        $this->queryAlbumID = $id;
    }
}



class albumByID extends RestHandler{
    var $albumID;

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from album where albumid = " . $this->albumID);
        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'ARTISTID', 'YEAR'));

        http_response_code(200);

        return $results;
    }

    function handle($method, $pathvar, $token){
        if(!intIsSane($token)){
            http_response_code(400);
            return "album id was not sanitary";
        }

        $this->albumID = $token;

        if(!$pathvar->isEmpty()){
            $this->objectmap['art'] = new albumArt($token);
            $this->objectmap['songs'] = new albumSongs($token);
        }
        return parent::handle($method, $pathvar, $token);
    }
}

class album extends RestHandler{
    function album(){
        $this->objectmap['/default'] = new albumByID();
    }

}
?>
