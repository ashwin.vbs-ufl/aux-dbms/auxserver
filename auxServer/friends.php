<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class friends extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select userid, name from userlist, friendlist where userlist.userid = friendlist.userid2 and friendlist.userid1 = '" . $_SESSION['USERID'] . "'");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);

        return $results;
    }

    function rest_put(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("insert into friendlist (userid1, userid2) values ('" . $_SESSION['USERID'] . "', '" . $request_json['userid'] . "')");

        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);

        return true;
    }

    function rest_delete(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("delete from friendlist where userid1 = '" . $_SESSION['USERID'] . "' and userid2 = '" . $request_json['userid'] . "'");

        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);

        return true;
    }
}

?>
