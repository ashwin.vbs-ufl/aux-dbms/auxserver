<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class libraryAlbums extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from album where albumid in (
                                    select albumid from library where userid = '" . $_SESSION['USERID'] . "')");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'ARTISTID', 'YEAR'));

        http_response_code(200);

        return $results;
    }
}

class libraryArtists extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from artist
                                    where artist.artistid in ((
                                        select artistid from performance where songid in (
                                            select songid from library where userid = '" . $_SESSION['USERID'] . "'))
                                    union (
                                        select artistid from album where albumid in (
                                            select albumid from library where userid = '" . $_SESSION['USERID'] . "')))");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ARTISTID', 'DOB'));

        http_response_code(200);

        return $results;
    }
}

class librarySongs extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from song
                                    where song.songid in (
                                        select songid from library where userid = '" . $_SESSION['USERID'] . "')");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'DATEOFPERFORMANCE'));

        http_response_code(200);

        return $results;
    }
}

class library extends RestHandler{
    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select library.albumid, library.songid, songinstance.tracknumber from library, songinstance where library.albumid = songinstance.albumid and library.songid = songinstance.songid and library.userid = '" . $_SESSION['USERID'] . "'");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'SONGID', 'TRACKNUMBER'));

        http_response_code(200);
        return $results;
    }

    function rest_put(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("insert into library (albumid, songid, userid) values (" . $request_json['ALBUMID'] . ", " . $request_json['SONGID'] . ", '" . $_SESSION['USERID'] . "')");

        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);

        return true;
    }

    function rest_delete(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("delete from library where albumid = " . $request_json['ALBUMID'] . " and songid = " . $request_json['SONGID'] . " and userid = '" . $_SESSION['USERID'] . "'");

        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);

        return true;
    }


    function library(){
        $this->objectmap['artists'] = new libraryArtists();
        $this->objectmap['albums'] = new libraryAlbums();
        $this->objectmap['songs'] = new librarySongs();
    }
}
?>
