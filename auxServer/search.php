<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class searchArtist extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $searchTerm = "artistname is not null ";

        foreach ($request_json['SearchTerms'] as $term){
            $searchTerm = $searchTerm . "and artistname like '%" . $term . "%' ";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from artist where " . $searchTerm);

        $results = $request->getResults(20);
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ARTISTID', 'DOB'));
        http_response_code(200);
        return $results;
    }
}

class searchAlbum extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $searchTerm = "albumname is not null ";

        foreach ($request_json['SearchTerms'] as $term){
            $searchTerm = $searchTerm . "and albumname like '%" . $term . "%' ";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from album where " . $searchTerm);

        $results = $request->getResults(20);
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ALBUMID', 'ARTISTID', 'YEAR'));
        http_response_code(200);
        return $results;
    }
}

class searchSong extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $searchTerm = "songname is not null ";

        foreach ($request_json['SearchTerms'] as $term){
            $searchTerm = $searchTerm . "and songname like '%" . $term . "%' ";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select * from songinstance where songid in (select songid from song where " . $searchTerm . ")");

        $results = $request->getResults(20);
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID', 'ALBUMID', 'TRACKNUMBER'));
        http_response_code(200);
        return $results;
    }
}

class searchAccount extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $searchTerm = "userid is not null ";

        foreach ($request_json['SearchTerms'] as $term){
            $searchTerm = $searchTerm . "and userid like '%" . $term . "%' ";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select userid, name from userlist where " . $searchTerm);

        $results = $request->getResults(20);
        $request->cleanup();
        $conn->cleanup();

        http_response_code(200);
        return $results;
    }
}

class search extends RestHandler{
    function search(){
        $this->objectmap['artist'] = new searchArtist();
        $this->objectmap['album'] = new searchAlbum();
        $this->objectmap['song'] = new searchSong();
        $this->objectmap['account'] = new searchAccount();
    }
}

?>
