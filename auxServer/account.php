<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';
require_once 'json/JSON.php';

class logout extends RestHandler{
    function rest_get(){
        unset($_SESSION["USERID"]);
        return true;
    }
}

class login extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        if(!stringIsSane($request_json['USERID'])){
            http_response_code(400);
            return "user id was not sanitary";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select USERID, SALT, PASSWORDHASH from userlist where USERID = '" . $request_json["USERID"] . "'");
        $record = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        if(count($record) == 0){
            http_response_code(401);
            return "userid not registered";
        }

        list($key, $val) = each($record);

        if(0 == strcmp($val['PASSWORDHASH'], sha1($request_json['PASSWORD'] . $val['SALT']))){
            http_response_code(200);
            $_SESSION["USERID"] = $request_json['USERID'];
            return true;
        } else {
            http_response_code(401);
            return "password incorrect";
        }
    }
}

class create extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        if( !stringIsSane($request_json['USERID']) ||
            !stringIsSane($request_json['NAME']) ||
            !stringIsSane($request_json['PASSWORD']) ||
            !stringIsSane($request_json['REGION']) ||
            !intIsSane($request_json['DOB'])){
            http_response_code(400);
            return "input data was not sanitary";
        }

        if( $request_json['USERID'] == "" ||
            $request_json['NAME'] == "" ||
            $request_json['REGION'] == "" ||
            $request_json['PASSWORD'] == "" ){
            http_response_code(400);
            return "input data was not sanitary";
        }

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $request->processRequest("select USERID, SALT, PASSWORDHASH from userlist where USERID = '" . $request_json['USERID'] . "'");
        $record = $request->getResults();
        $request->cleanup();

        if(count($record) > 0){
            $conn->cleanup();
            http_response_code(409);
            return "userid already exists";
        }

        $salt = uniqid("_");
        $passwordhash = sha1($request_json['PASSWORD'] . $salt);
        $request = $conn->getQueryPrimitive();
        $res = $request->processRequest(   "insert into userlist(USERID, NAME, DOB, REGION, SALT, PASSWORDHASH) values ('" .
                                            $request_json['USERID'] . "', '" .
                                            $request_json['NAME'] . "', " .
                                            $request_json['DOB'] . ", '" .
                                            $request_json['REGION'] . "', '" .
                                            $salt . "', '" .
                                            $passwordhash . "')" );

        $request->cleanup();
        $conn->cleanup();

        if(!$res){
            http_response_code(500);
            return "database insert failed";
        }

        http_response_code(200);
        return true;
    }
}

class status extends RestHandler{
    function rest_get(){
        return isset($_SESSION['USERID']);
    }
}

class accountByID extends RestHandler{
    var $accountID = "";

    function rest_get(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();
        $results = NULL;
        if($this->accountID == $_SESSION['USERID']){
            $request->processRequest("select USERID, NAME, DOB from userlist where USERID = '" . $this->accountID . "'");
            $results = $request->getResults();
            fixIntegerColumns($results, array('DOB'));

        } else {
            $request->processRequest("select USERID, NAME from userlist where USERID = '" . $this->accountID . "'");
            $results = $request->getResults();
        }
        $request->cleanup();
        $conn->cleanup();

        if(count($results) > 0)
            http_response_code(200);
        else
            http_response_code(404);

        return $results;
    }

    function handle($method, $pathvar, $token){
        if(!stringIsSane($token)){
            http_response_code(400);
            return "user id was not sanitary";
        }

        $this->accountID = $token;
        return parent::handle($method, $pathvar, $token);
    }
}

class account extends RestHandler{
    function account(){
        $this->RestHandler();
        $this->objectmap['login'] = new login();
        $this->objectmap['create'] = new create();
        $this->objectmap['status'] = new status();
        $this->objectmap['logout'] = new logout();

        if(isset($_SESSION['USERID'])){
            $this->objectmap['/default'] = new accountByID();
        }
    }
}

?>
