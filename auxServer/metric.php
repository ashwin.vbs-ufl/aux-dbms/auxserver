<?php
require_once 'librest/RestHandler.php';
require_once 'error/errorCodes.php';
require_once 'oraclesql/OCI.php';
require_once 'dbDetails.php';

class regionMetric extends RestHandler{
    function rest_post(){
        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();

        $request->processRequest("select region, songid
                                    from (
                                      select distinct region, songid, row_number() over(partition by region order by cnt desc) as rn, cnt
                                      from (
                                        select distinct region, songid, count(time) over(partition by region, songid order by region) as cnt
                                        from userlist join history on userlist.userid = history.userid
                                      )
                                    ) where rn = 1 ");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('SONGID'));

        http_response_code(200);

        return $results;
    }
}

class periodMetric extends RestHandler{
    function rest_post(){
        $json = new Services_JSON();
        $request_json = (array)($json->decode(file_get_contents('php://input')));

        $conn = new OCI(DB_SERVER, DB_USER, DB_PASSWORD);
        $request = $conn->getQueryPrimitive();

        $request->processRequest("select rownum, songid from (select * from (select count(time) as cnt, songid from history where time>" . $request_json['minTime'] . " and time<" . $request_json['maxTime'] . " group by songid) order by cnt desc) where rownum<11");

        $results = $request->getResults();
        $request->cleanup();
        $conn->cleanup();

        fixIntegerColumns($results, array('ROWNUM', 'SONGID'));

        http_response_code(200);

        return $results;
    }
}

class metric extends RestHandler{
    function metric(){
        $this->objectmap['region'] = new regionMetric();
        $this->objectmap['period'] = new periodMetric();
    }
}
?>
