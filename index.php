#!/usr/bin/php
<?php
require_once './auxServer/account.php';
require_once './auxServer/album.php';
require_once './auxServer/artist.php';
require_once './auxServer/friends.php';
require_once './auxServer/library.php';
require_once './auxServer/rating.php';
require_once './auxServer/song.php';
require_once './auxServer/search.php';
require_once './auxServer/metric.php';


require_once 'librest/Path.php';
require_once 'json/JSON.php';

session_start();
header('Content-Type: application/json');


$path = new PathParser($_REQUEST['path']);

 class base extends RestHandler{
 	function base(){
        $this->objectmap['account'] = new account();
        $this->objectmap['album'] = new album();
        $this->objectmap['artist'] = new artist();
        $this->objectmap['song'] = new song();
        if(isset($_SESSION['USERID'])){
            $this->objectmap['friends'] = new friends();
            $this->objectmap['library'] = new library();
            $this->objectmap['metric'] = new metric();
            // $this->objectmap['playlist'] = new playlist();
            $this->objectmap['rating'] = new rating();
            $this->objectmap['search'] = new search();
        }
 	}
 }

$keystore = new base();
$json = new Services_JSON();
$result = array();
$result['result'] = $keystore->handle($_SERVER['REQUEST_METHOD'], $path);
echo $json->encode($result);

?>
